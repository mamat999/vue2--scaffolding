import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: '登录',
    component: () => import( '../views/Login.vue')
  },
  {
    path: '/register',
    name: '注册',
    component: () => import( '../views/Register.vue')
  },
  {
    path: '/home',
    name: '主页',
    component: () => import( '../views/Home.vue'),
    redirect:'/person',
    children:[
      {
        path: '/person',
        name: '个人中心',
        component: () => import( '../views/Person.vue'),
      },
      {
        path: '/about',
        name: '关于我',
        component: () => import( '../views/About.vue'),
      },
      {
        path: '/echars',
        name: '首页',
        component: () => import( '../views/Echars.vue'),
      }
    ]
  },
  {
    path: '/aside',
    name: '侧栏',
    component: () => import( '../components/Aside.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// currentPathName路由设置
router.beforeEach((to,from,next)=>{
  localStorage.setItem("currentPathName",to.name)
  store.commit("setPath")
  // const storeMenus= localStorage.getItem("menus")
  // 未找到路由的情况
  // if(!to.matched.length){
  //   // 查看是否登录了没有，如果没有跳转到登录页面
  //   if(storeMenus){
  //     next("/404")
  //   }else{
  //     next("/")
  //   }
  // }
  next()
})
export default router
