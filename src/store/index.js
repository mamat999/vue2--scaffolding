import Vue from 'vue'
import Vuex from 'vuex'
import router, { resetRouter } from '@/router';
Vue.use(Vuex)

// currentPathName路由设置
export default new Vuex.Store({
  state: {
    currentPathName: '',
  },
  mutations: {
    setPath(state){
        state.currentPathName=localStorage.getItem("currentPathName")
    },
    logout(state){
      // 清空缓存
      localStorage.removeItem("user")
      localStorage.removeItem("menus")
      router.push("/")
      // 重置路由
      resetRouter()
    }
    // 定义修改状态的方法
    // increment(state) {
    //   state.count++;
    // },
    // decrement(state) {
    //   state.count--;
    // },
  },
  getters: {
    // 定义派生状态
    getCount(state) {
      return state.count;
    },
  },

  actions: {
    // 定义操作触发 mutation 的 actions
    increment(context) {
      context.commit('increment');
    },
    decrement(context) {
      context.commit('decrement');
    },
  },
  modules: {
  }
})
